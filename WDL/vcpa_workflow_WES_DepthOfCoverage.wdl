#####
# Created: May-01-2017
# Stage1  Check data-quality of mapping (pre-VCF check)
#         Sambamba depth
#####

import "WDL/stage1/wes_depthOfCoverage.wdl" as wes_depthOfCoverage

workflow Tutorial_vcpa_steps {

###
# Input requirements
# Use tools are included SAMBAMBA, SAMTOOLS
# File is from input_bam
# Library is on VCPA_LIB
###

  File GATK
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String VCPA_LIB
  String TARGET


  call wes_depthOfCoverage.wes_depthOfCoverage   {

         input:
         GATK = GATK,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         TARGET = TARGET,
         VCPA_LIB = VCPA_LIB
  }


  meta {
    version: "1.0.0"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}
