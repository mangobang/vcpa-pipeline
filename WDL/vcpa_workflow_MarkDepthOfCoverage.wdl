#####
# Created: May-01-2017
# Stage1  markdepthOfCoverage for API
#####

import "WDL/stage1/markdepthOfCoverage.wdl" as markdepthOfCoverage

workflow Tutorial_vcpa_steps {

###
# Input requirements
# File is from input_bam
# Library is on VCPA_LIB
###

  File input_bam
  String tmp
  String log
  String RG_results
  String VCPA_LIB


  call markdepthOfCoverage.markdepthOfCoverage   {

         input:
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         VCPA_LIB = VCPA_LIB
  }


  meta {
    version: "1.0.0"
  }
}
