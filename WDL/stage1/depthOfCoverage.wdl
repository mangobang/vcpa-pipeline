#####
# Created: May-01-2017
# Stage1  Check data-quality of mapping (pre-VCF check)
#         Sambamba depth
#####


task depthOfCoverage {

###
# Input requirements
# Use tools are included SAMBAMBA, SAMTOOLS
# File is from input_bam
# Library is on VCPA_LIB
###

  File SAMTOOLS
  File SAMBAMBA
  String RG_results
  String tmp
  String log
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")
  String chr
  String VCPA_LIB

  command {

  #  count depthOfCoverage between 5x, 10x, 20x, 30x, 40x, 50x coverage

       if [ ! -s "${RG_results}/bam/$FILENAME.depth.txt" ];then

        FILENAME=`basename ${file_basename}.bam .bam`
        qsub \
            -N depth-$FILENAME-chr${chr}   \
            -cwd  \
            -o ${log}/depthOfCoverage-${chr}.log \
            -l h_vmem=2G \
            -V \
            -hold_jid markDup* \
            -j y ${VCPA_LIB}/stage1/depthOfCoverage.sh -i ${RG_results}/bam/$FILENAME.sorted.dupmarked.bam -l ${chr}  -p "${RG_results}/bam/$FILENAME"  -b "${VCPA_LIB}/stage0"


       fi
  }
  output {
  String response = read_string(stdout())
  }
}
