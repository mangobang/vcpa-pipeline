#####
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)
#         Samtools sort
#####

task markDupstart {

###
# Input requirements
# Use tools are included SAMBAMBA, bamUtil
# File is from input_bam
# Library is on VCPA_LIB
###

  File SAMBAMBA
  File bamUtil
  String VCPA_LIB
  String tmp
  String log
  String RG_results
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")

  command {
  # sorts the input file by query-name using samtools,
  # streammed to samblaster for adding in MC/MQ tags
  # output coordinate sorted BAM with MC/MQ tags
  # @param BAM - input BAM file
  # @output BAM - coordinate-sorted BAM


   FILENAME=`basename ${file_basename}.bam .bam`
   #MEM=`bash ${VCPA_LIB}/maxmem.sh -i ${file_basename}.bam`

   INPUT=""
   if [ -s "${tmp}/bam/$FILENAME.sorted.bam" ];then
      INPUT="${tmp}/bam/$FILENAME.sorted.bam"
   else
      INPUT="${tmp}/bam/$FILENAME.bam"
   fi

   if [ ! -s "${RG_results}/bam/$FILENAME.sorted.dupmarked.bam" ];then
          qsub \
            -N  markDupstart-$FILENAME \
            -cwd  \
            -o ${log}/markdup.log \
            -V \
            -j y ${VCPA_LIB}/stage1/markDup-start.sh -i "$INPUT" -p "${RG_results}/bam/$FILENAME" -b "${VCPA_LIB}/stage0" -m "${RG_results}"

    fi

  }
  output {
  String response = read_string(stdout())
  }
}
