#####
# Created: April-19-2018
# Stage1 Count reads with mapping quality >=30
#         Sambamba
#####

task mq30 {

###
# Input requirements
# Use tools are included SAMBAMBA
# File is from input_bam
# Library is on VCPA_LIB
###


  File SAMBAMBA
  String VCPA_LIB
  String tmp
  String log
  String RG_results
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")

  command {
  # sorts the input file by query-name using samtools,
  # streammed to samblaster for adding in MC/MQ tags
  # output coordinate sorted BAM with MC/MQ tags
  # @param BAM - input BAM file
  # @output BAM - coordinate-sorted BAM


   FILENAME=`basename ${file_basename}.bam .bam`
   #MEM=`bash ${VCPA_LIB}/maxmem.sh -i ${file_basename}.bam`
 

          qsub \
            -N mq30-$FILENAME \
            -cwd  \
            -o ${log}/mq30.log \
            -V \
            -hold_jid markDupAll-$FILENAME  \
            -j y ${VCPA_LIB}/stage1/mq30.sh -i ${RG_results}/bam/$FILENAME.sorted.dupmarked.bam  -p "${RG_results}/bam/$FILENAME" -b "${VCPA_LIB}/stage0" -m "${tmp}/bam/$FILENAME"


  }
  output {
  String response = read_string(stdout())
  }
}
