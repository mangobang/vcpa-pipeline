#####
# Created: May-01-2017
#Stage2a Recalibrate reads on BAMs
#         GATK BaseRecalibrator
#####


task bcal {


###
# Input requirements
# Use tools are included GATK
# File is from input_bam
# Library is on VCPA_LIB, KNOWN, DBSNP, GOLD, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File GATK
  String tmp
  String log
  String THREADS
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File KNOWN
  File KNOWN_index
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index
  String file_basename = sub(input_bam, "\\.bam", "")
  String VCPA_LIB
  String RG_results


  command {

     FILENAME=`basename ${file_basename}.bam .bam`
     #MEM=`bash ${VCPA_LIB}/maxmem.sh -i ${file_basename}.bam`
     #THREADS=`bash ${VCPA_LIB}/bwathreads.sh -i ${file_basename}.bam`

     if [ ! -s "${RG_results}/bam/$FILENAME.recal_data.table" ];then
        qsub \
            -N BCal-$FILENAME  \
            -cwd  \
            -o ${log}/recal.log \
            -V \
            -hold_jid markDupAll-$FILENAME \
            -j y ${VCPA_LIB}/stage2a/bcal.sh -i "${RG_results}/bam/$FILENAME.sorted.dupmarked.bam" -b "${VCPA_LIB}/stage0" -p "${RG_results}/bam/$FILENAME" -t ${THREADS} -m "${RG_results}"


     fi
  }

  output {
    #File bal_table  = "${file_basename}.recal_data.table"
    String response = read_string(stdout())
  }
}
