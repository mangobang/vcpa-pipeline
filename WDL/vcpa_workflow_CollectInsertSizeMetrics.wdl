#####
# Created: April-19-2018
# Stage2b 
#         PICARD CollectInsertSizeMetrics
#####

import "WDL/stage2b/collectInsertSize.wdl" as collectInsertSize

workflow Tutorial_vcpa_steps {


###
# Input requirements
# Use tools are included PICARD
# File is from input_bam
# Library is on VCPA_LIB
###

  File PICARD
  File input_bam
  String tmp
  String log
  String RG_results
  String VCPA_LIB


  call collectInsertSize.collectInsertSize   {

         input:
         PICARD = PICARD,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         VCPA_LIB = VCPA_LIB
  }



  meta {
    version: "1.0.0"
    GATK: "3.7"
  }
}