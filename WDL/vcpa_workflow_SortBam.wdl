#####
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)
#         Samtools sort
#####

import "WDL/stage1/sortBam.wdl" as sortBam

workflow Tutorial_vcpa_steps {

###
# Input requirements
# Use tools are included SAMBLASTER, SAMTOOLS
# File is from input_bam
# Library is on VCPA_LIB
###
  File SAMBLASTER
  File SAMTOOLS
  File input_bam
  String VCPA_LIB
  String tmp
  String log
  String RG_results


  call sortBam.sortBam   {

         input:
         SAMTOOLS = SAMTOOLS,
         SAMBLASTER = SAMBLASTER,
         input_bam = input_bam,
         VCPA_LIB = VCPA_LIB,
         tmp = tmp,
         log = log,
         RG_results = RG_results

  }

  meta {
    version: "1.0.0"
    SAMBLASTER: "0.1.24"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}
