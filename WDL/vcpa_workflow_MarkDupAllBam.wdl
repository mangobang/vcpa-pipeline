#####
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)
#         Samtools sort
#####

import "WDL/stage1/markDupAllBam.wdl" as markDupAllBam

workflow Tutorial_vcpa_steps {

###
# Input requirements
# Use tools are included SAMBAMBA, SAMTOOLS, bamUtil
# File is from input_bam
# Library is on VCPA_LIB
###

  File SAMBAMBA
  File SAMTOOLS
  File bamUtil
  File input_bam
  String tmp
  String log
  String RG_results
  String VCPA_LIB


  call markDupAllBam.markDupAllBam  {

         input:
         SAMTOOLS = SAMTOOLS,
         SAMBAMBA = SAMBAMBA,
         bamUtil = bamUtil,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         VCPA_LIB = VCPA_LIB
  }


  meta {
    version: "1.0.0"
    BAMUTILmem: "1.0.14"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}
