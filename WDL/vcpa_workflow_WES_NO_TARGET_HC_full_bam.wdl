#####
# Created: May-01-2017
# Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS)
#         GATK HaplotypeCaller
#####

import "WDL/stage2b/wes_no_target_hc_full_bam.wdl" as wes_no_target_hc_full_bam

workflow Tutorial_vcpa_steps {


###
# Input requirements
# Use tools are included GATK
# File is from input_bam
# Library is on VCPA_LIB, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File GATK
  File input_bam
  String tmp
  String log
  String RG_results
  String VCPA_LIB
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai


  call wes_no_target_hc_full_bam.wes_no_target_hc_full_bam   {

         input:
         GATK = GATK,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         ref_fasta = ref_fasta,
         ref_dict = ref_dict,
         ref_alt = ref_alt,
         ref_amb = ref_amb,
         ref_ann = ref_ann,
         ref_bwt = ref_bwt,
         ref_pac = ref_pac,
         ref_sa = ref_sa,
         ref_fai = ref_fai,
         RG_results = RG_results,
         VCPA_LIB = VCPA_LIB
  }


  meta {
    version: "1.0.0"
    GATK: "3.7"
  }
}
