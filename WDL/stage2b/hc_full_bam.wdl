#####
# Created: May-01-2017
#Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS)
#         GATK HaplotypeCaller
#####


task hc_full_bam {


###
# Input requirements
# Use tools are included GATK
# File is from input_bam
# Library is on VCPA_LIB, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File GATK
  String tmp
  String log
  String RG_results
  String VCPA_LIB
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  String file_basename = sub(input_bam, "\\.bam", "")


  command {

  # run GATK HaplotypeCaller on a region of input bam to generate a GVCF
  # @param (-i INPUTBAM) - input SEQ FILE
  # @param (-c CHR)      - region for "-L" interval flag
  # @param (-p PREFIX)   - out-file naming prefix
  # @param (-n)          - override PCRMODEL from CONSERVATIVE to NONE, required for PCR-free data
  # @return OUTFILE - named as $PREFIX.$CHR.g.vcf.gz

   mkdir -p ${RG_results}/vcf
   #RMEM="23.5G"
   #export XMEM="17g"
   chrlists=`seq -f "chr%g" 1 22`
   for CHR in $chrlists chrX chrY chrM;do
    if [ ! -s "${file_basename}.$CHR.g.vcf.gz" ];then

     THREADS=2
     PRI=100
     if [ $CHR == "chr1" ] || [ $CHR == "chr2" ] || [ $CHR == "chr12" ] || [ $CHR == "chr6" ];then
        THREADS=2
        PRI=900
     elif [ $CHR == "chr3" ] || [ $CHR == "chr5" ] || [ $CHR == "chr13" ];then
        THREADS=2
        PRI=900
     elif [ $CHR == "chr9" ];then
        THREADS=2
        PRI=900
     elif [ $CHR == "chr14" ];then
        THREADS=1
        PRI=900
     fi

     if [ $CHR == "chr17" ];then
        THREADS=2
        PRI=800
     elif [ $CHR == "chr11" ];then
        THREADS=1
        PRI=750
     elif [ $CHR == "chr18" ];then
        THREADS=1
        PRI=700
     fi

     if [ $CHR == "chr16" ];then
        THREADS=2
        PRI=650
     elif [ $CHR == "chr10" ];then
        THREADS=2
        PRI=600
     elif [ $CHR == "chr19" ];then
        THREADS=1
        PRI=550
     elif [ $CHR == "chr8" ];then
        THREADS=2
        PRI=500
     elif [ $CHR == "chr22" ];then
        THREADS=1
        PRI=450
     elif [ $CHR == "chr15" ];then
        THREADS=2
        PRI=400
     elif [ $CHR == "chr21" ];then
        THREADS=1
        PRI=350
     elif [ $CHR == "chr4" ];then
        THREADS=2
        PRI=300
     elif [ $CHR == "chr20" ];then
        THREADS=2
        PRI=250
     elif [ $CHR == "chr7" ];then
        THREADS=2
        PRI=200
     elif [ $CHR == "chrX" ];then
        THREADS=5
        PRI=100
     elif [ $CHR == "chrY" ];then
        PRI=100
        #RMEM="27G"
        #export XMEM="21g"
     elif [ $CHR == "chrM" ];then
        THREADS=1
        PRI=100
        #RMEM="31G"
        #export XMEM="25g"
     fi
         FILENAME=`basename ${file_basename}.bam .bam`
         MEM=`bash ${VCPA_LIB}/hcmem.sh -i ${file_basename}.bam -t $THREADS -m $RMEM`
         echo $MEM >> ${file_basename}.hc-$CHR.log
         qsub \
            -N HC-$FILENAME-$CHR \
            -cwd  \
            -o ${log}/hc-$CHR.log \
            -V \
            -pe DJ $THREADS \
            -p $PRI \
            -hold_jid IndelRealign-merge-$FILENAME \
            -j y ${VCPA_LIB}/stage2b/hc_full_bam.sh -i "${RG_results}/bam/$FILENAME.hg38.realign.bqsr.bam" -b "${VCPA_LIB}/stage0" -p "${RG_results}/vcf/$FILENAME" -t $THREADS -m "${RG_results}" -l "$CHR"

    fi

   done

           #qsub \
           # -N HC-upload \
           # -cwd  \
           # -o ${log}/hc-upload.log \
           # -V \
           # -pe DJ $THREADS \
           # -hold_jid HC-$FILENAME-chr*  \
           # -j y ${VCPA_LIB}/stage2b/markCompletedHC.sh -i "${RG_results}/bam/$FILENAME.hg38.realign.bqsr.bam" -b "${VCPA_LIB}/stage0" -p "${RG_results}/vcf/$FILENAME"  -m "${log}"




  }

  output {
  #File hap_vcf = "${file_basename}.$CHR.g.vcf.gz"
   String response = read_string(stdout())
  }
}
