#####
# Created: May-01-2017
# Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS)
#         GATK HaplotypeCaller
#####


task wes_no_target_hc_full_bam {


###
# Input requirements
# Use tools are included GATK
# File is from input_bam
# Library is on VCPA_LIB, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File GATK
  String tmp
  String log
  String RG_results
  String VCPA_LIB
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  String file_basename = sub(input_bam, "\\.bam", "")


  command {

  # run GATK HaplotypeCaller on a region of input bam to generate a GVCF
  # @param (-i INPUTBAM) - input SEQ FILE
  # @param (-c CHR)      - region for "-L" interval flag
  # @param (-p PREFIX)   - out-file naming prefix
  # @param (-n)          - override PCRMODEL from CONSERVATIVE to NONE, required for PCR-free data
  # @return OUTFILE - named as $PREFIX.$CHR.g.vcf.gz

   mkdir -p ${RG_results}/vcf
   #RMEM="23.5G"
   THREADS="4"
   #export XMEM="17g"


         FILENAME=`basename ${file_basename}.bam .bam`
         #MEM=`bash ${VCPA_LIB}/hcmem.sh -i ${file_basename}.bam -t $THREADS -m $RMEM`
         #echo $MEM >> ${file_basename}.hc.log
         qsub \
            -N HC-$FILENAME \
            -cwd  \
            -o ${log}/hc-wes.log \
            -V \
            -pe DJ $THREADS \
            -hold_jid IndelRealign-merge-$FILENAME \
            -j y ${VCPA_LIB}/stage2b/wes_no_target_hc_full_bam.sh -i "${RG_results}/bam/$FILENAME.hg38.realign.bqsr.bam" -b "${VCPA_LIB}/stage0" -p "${RG_results}/vcf/$FILENAME" -t $THREADS -m "${RG_results}"

    

           qsub \
            -N HC-upload \
            -cwd  \
            -o ${log}/hc-upload.log \
            -V \
            -pe DJ $THREADS \
            -hold_jid HC-$FILENAME  \
            -j y ${VCPA_LIB}/stage2b/markCompletedHC.sh -i "${RG_results}/bam/$FILENAME.hg38.realign.bqsr.bam" -b "${VCPA_LIB}/stage0" -p "${RG_results}/vcf/$FILENAME"  -m "${log}"




  }

  output {
  #File hap_vcf = "${file_basename}.$CHR.g.vcf.gz"
   String response = read_string(stdout())
  }
}
