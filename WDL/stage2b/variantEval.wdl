#####
# Created: May-01-2017
#Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS)
#         GATK VariantEval
#####


task variantEval {


###
# Input requirements
# Use tools are included GATK
# File is from input_bam
# Library is on VCPA_LIB, DBSNP, GOLD, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File GATK
  String tmp
  String log
  String THREADS
  String RG_results
  String VCPA_LIB
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index
  String file_basename = sub(input_bam, "\\.bam", "")


  command {

  FILENAME=`basename ${file_basename}.bam .bam`
  chrlists=`seq -f "chr%g" 1 22`
  for CHR in $chrlists chrX chrY chrM;do
    MSTR+="--eval ${RG_results}/vcf/$FILENAME.$CHR.g.vcf.gz "
  done

    if [ ! -s "${RG_results}/vcf/eval.$FILENAME.txt" ];then

         #MEM=`bash ${VCPA_LIB}/vemem.sh -i ${file_basename}.bam -t 7`
         qsub \
            -N VariantEval-$FILENAME \
            -cwd  \
            -o ${log}/ve.log \
            -V \
            -pe DJ 2 \
            -hold_jid "HC-$FILENAME-*" \
            -j y ${VCPA_LIB}/stage2b/variantEval.sh -i "$MSTR" -b "${VCPA_LIB}/stage0" -p "${RG_results}/vcf/$FILENAME" -t 7 -m "${RG_results}" -l ${log}

    fi

  }

  output {
  #File eval_vcf = "${file_basename}.eval.txt"
   String response = read_string(stdout())
  }
}
