#####
# Created: July-31-2017
# Stage0 Roll back and Map reads to reference genome
#         SAMBAMBA
#         SAMTOOLS
#####

import "WDL/stage0/sambambareadname_sort.wdl" as SAMBAMBAreadname_sort

workflow Tutorial_vcpa_steps {

###
# Input requirements
# Use tools are included SAMBAMBA, SAMTOOLS
# File is from input_bam
# Library is on VCPA_LIB
###

  File SAMBAMBA
  File SAMTOOLS
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String VCPA_LIB

  call SAMBAMBAreadname_sort.SAMBAMBAreadname_sort   {

         input:
         SAMBAMBA = SAMBAMBA,
         SAMTOOLS = SAMTOOLS,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         THREADS = THREADS,
         RG_results = RG_results,
         VCPA_LIB = VCPA_LIB
  }


  meta {
    version: "1.0.0"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
  }
}
