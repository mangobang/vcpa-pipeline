#####
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         Sambamba merge
#         Sambamba index
#####

import "WDL/stage2a/mergeindelRealigner.wdl" as mergeindelRealigner

workflow Tutorial_vcpa_steps {


###
# Input requirements
# Use tools are included SAMTOOLS, SAMBAMBA, SAMBLASTER
# File is from input_bam
# Library is on VCPA_LIB, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File SAMTOOLS
  File SAMBAMBA
  File SAMBLASTER
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String VCPA_LIB
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai


  call mergeindelRealigner.mergeindelRealigner  {

         input:
         SAMTOOLS = SAMTOOLS,
         SAMBAMBA = SAMBAMBA,
         SAMBLASTER = SAMBLASTER,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         THREADS = THREADS,
         ref_fasta = ref_fasta,
         ref_dict = ref_dict,
         ref_alt = ref_alt,
         ref_amb = ref_amb,
         ref_ann = ref_ann,
         ref_bwt = ref_bwt,
         ref_pac = ref_pac,
         ref_sa = ref_sa,
         ref_fai = ref_fai,
         RG_results = RG_results,
         VCPA_LIB = VCPA_LIB
  }



  meta {
    version: "1.0.0"
    SAMBAMBA: "0.6.5"
    SAMTOOLS: "1.3.1-42-g0a15035"
    SAMBLASTER: "0.1.24"
  }
}
