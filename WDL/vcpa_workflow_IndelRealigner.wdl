#####
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         GATK IndelRealigner
#####

import "WDL/stage2a/indelRealigner.wdl" as indelRealigner

workflow Tutorial_vcpa_steps {


###
# Input requirements
# Use tools are included GATK
# File is from input_bam
# Library is on VCPA_LIB, KNOWN, DBSNP, GOLD, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File GATK
  File input_bam
  String tmp
  String log
  String THREADS
  String RG_results
  String VCPA_LIB
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File KNOWN
  File KNOWN_index
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index


  call indelRealigner.indelRealigner   {

         input:
         GATK = GATK,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         THREADS = THREADS,
         ref_fasta = ref_fasta,
         ref_dict = ref_dict,
         ref_alt = ref_alt,
         ref_amb = ref_amb,
         ref_ann = ref_ann,
         ref_bwt = ref_bwt,
         ref_pac = ref_pac,
         ref_sa = ref_sa,
         ref_fai = ref_fai,
         KNOWN = KNOWN,
         KNOWN_index = KNOWN_index,
         DBSNP = DBSNP,
         DBSNP_index = DBSNP_index,
         GOLD = GOLD,
         GOLD_index = GOLD_index,
         VCPA_LIB = VCPA_LIB,
         RG_results = RG_results

  }


  meta {
    version: "1.0.0"
    GATK: "3.7"
  }
}
