# Step 6: Submit a job to process one whole genome \(WGS\) / whole exome \(WES\) sample

**After setting up the tracking database, users are ready to process one WGS / WES sample. Please follow these steps below:**

[**6.1 Update vcpa-pipeline bitbucket contents**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/master/doc/step-6-submit-a-job-to-process-one-whole-genome-wgs-whole-exome-wes-sample/6.1-update-vcpa-pipeline-bitbucket-contents.md)

[**6.2 Choose which workflow to use**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/master/doc/step-6-submit-a-job-to-process-one-whole-genome-wgs-whole-exome-wes-sample/6.2-choose-which-workflow-to-use.md)

[**6.3 Enter your AWS credentials into the workflow script**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/master/doc/step-6-submit-a-job-to-process-one-whole-genome-wgs-whole-exome-wes-sample/6.3-enter-your-aws-credentials-into-the-workflow-script.md)

[**6.4 Launch Amazon EC2 Spot Instances via starcluster**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/master/doc/step-6-submit-a-job-to-process-one-whole-genome-wgs-whole-exome-wes-sample/6.4-launch-amazon-ec2-spot-instances-via-starcluster.md)

