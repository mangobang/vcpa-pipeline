
----
# description:
  Before running the VCPA pipeline, users will need to follow the steps in this session to set up the AWS environment.
----

# Step 1: Set up the Amazon Web Services \(AWS\) environment

[**1.1 Create AWS account**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/014d81d81c2b7eafd351251a1d09aad4e7a77ba5/doc/step-1-set-up-the-amazon-web-services-aws-environment/1.1-create-aws-account.md?at=master)

[**1.2 Configure your computing environment and login to AWS**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/014d81d81c2b7eafd351251a1d09aad4e7a77ba5/doc/step-1-set-up-the-amazon-web-services-aws-environment/1.2-configure-your-computing-environment-and-login-to-aws.md?at=master)

[**1.3 Setup S3 bucket \(simple storage solution for AWS\) for hosting sequencing data**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/014d81d81c2b7eafd351251a1d09aad4e7a77ba5/doc/step-1-set-up-the-amazon-web-services-aws-environment/1.3-setup-a-s3-bucket-simple-storage-solution-for-aws-for-hosting-sequencing-data.md?at=master)

[**1.4 Install AWS command line software and link your S3 bucket to your S3 account**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/014d81d81c2b7eafd351251a1d09aad4e7a77ba5/doc/step-1-set-up-the-amazon-web-services-aws-environment/1.4-install-aws-command-line-software-for-accessing-s3-bucket-via-command-line-interface.md?at=master)

[**1.5 Install StarCluster for AWS instance provisioning \(optional\)**](https://bitbucket.org/NIAGADS/vcpa-pipeline/src/014d81d81c2b7eafd351251a1d09aad4e7a77ba5/doc/step-1-set-up-the-amazon-web-services-aws-environment/1.5-install-starcluster-for-aws-instance-provisioning-optional.md?at=master)
