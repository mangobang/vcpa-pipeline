# 3.2 Create the project in the tracking database

#### After login to the tracking database, users can add a new project into the database by using the [command](https://curl.haxx.se/docs/manpage.html) below:

```text
curl -sS 
-d project_name=${project_name} 
-d project_name_full=${project_name_full} 
-d project_desc=${projectdesc} 
-d project_center=${project_center} 
-d subproject_name=${subproject_name} "http://IP/v1/projects/add"
```


![](../.gitbook/assets/3.2-1.png)




Hint: **The VCPA API will directly return a project ID \(project\_id\) for this project.**


