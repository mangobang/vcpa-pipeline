# 5.1 Input the sample information to the tracking database

**After logging into the tracking database, users can upload the list of samples to be processed into the database using the** [**command**](https://curl.haxx.se/docs/manpage.html) **below:**

```text
 curl -sS 
 -d project_id=${project_id} 
 -d sample_name=${sample_name} 
 -d project_desc=${project_desc} 
 -d subject_name=${subject_name} 
 -d LSAC=${LSAC} 
 -d seq_type=${seq_type} "http://IP/v1/projects/sample/add"
```

![](../.gitbook/assets/5.1-1.png)

