# 5.3 Populate the tracking database with the designated result folder for each sample to be processed

**Besides populating the input file S3 path, users will also need to set the results folder using the API command VCPA provides, so that this information will be effectively imported to the tracking database:**

```text
function rawurlencode(){
	echo -n "$1" | perl -pe 's/([^a-zA-Z0-9_.!~*()'\''-])/sprintf("%%%02X", ord($1))/ge' 
	| perl -pe 's/(\W)/sprintf("%%%02X", ord($1))/ge'}
```

```text

$  s3='s3://YOUR/RESULTS/FOLDER'    			### S3 path for the results folder for each sample 
$  results_s3_path=$(rawurlencode $s3)
```

```text
$ curl -sS "http://IP/v1/sample/set-attr/results_s3_uri/${project_id}/${sample_name}/${results_s3_path}"
```

![](../.gitbook/assets/5.3-1.png)
