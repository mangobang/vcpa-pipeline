#!/bin/bash

export DIR="/mnt/adsp"
source $DIR/VCPA/stage0/pipeline.ini
source $DIR/VCPA/stage0/common_functions.sh
source $DIR/VCPA/stage0/seqfile_functions.sh

while getopts di: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLE="$OPTARG";;
   esac
done

 MEM=$(get_max_mem)
 echo "${MEM}"
