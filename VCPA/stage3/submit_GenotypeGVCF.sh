#!/bin/bash

source /mnt/adsp/pipeline.ini
source /mnt/adsp/lib/common_functions.sh

while getopts dh: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
   esac
done

QSUB=qsub
if [ "$DODEBUG" ];then
 QSUB="echo qsub"
fi
BASE=/mnt/adsp/test_tileDB

SINGLE=1

if [ "$SINGLE" != "1" ];then

  for CHR in X {1..22} Y M;do

        if [ -s $BASE/GSTEP-GATK37/*.hg38.tileDB.chr$CHR.genotypedGVCF.g.vcf.gz.tbi ] || [ $(qstat -xml -u '*' | grep -Pc "ADSP-ADNI-GP.chr$CHR\b") -gt 0 ] ;then
            continue
        fi


        if [ -s $BASE/*/pVCF/CHR$CHR.part0.g.vcf.gz.tbi ];then
            P="$BASE/*/pVCF/CHR"
        else
            P="$BASE/*/spillover/CHR"
        fi

        THREADS=$(ls $P$CHR.*gz|wc -l)
        THREADS=$[$THREADS/2 + 1]


        MEM=$(adjustWorkingMem 15G $THREADS)
        $QSUB -pe DJ $THREADS \
              -notify \
              -cwd \
              -l h_vmem=$MEM \
              -N "*-GP.chr$CHR" \
              -o $BASE/Logs/*.GATK37.mtGP.chr$CHR.log \
              -V \
              -j y \
              $BASE/GenotypeGVCF_tileDB.sh -p $P -c "$CHR" -o $BASE/GSTEP-GATK37/*.hg38.tileDB.chr$CHR.genotypedGVCF.g.vcf.gz -t $THREADS

  done

else

   for GVCF in $BASE/pVCF_genomicsDB/CHR*g.vcf.gz;do

        if [ ! -s ${GVCF}.tbi ];then
            continue
        fi


        PART=$(echo $GVCF|grep -Po "part\d+")
        PART=${PART/part/}
        CHR=$(echo $GVCF|grep -Po "CHR[^\.]+")
        CHR=${CHR/CHR/}

        if [ $(qstat -xml -u '*' | grep -Pc "*-GP-c$CHR-p$PART\b") -gt 0 ] || [ -s $BASE/GSTEP-GATK37/*.hg38.tileDB.c$CHR-$PART.genotypedGVCF.g.vcf.gz.tbi ];then
            continue
        fi

        THREADS=1
        MEM=$(adjustWorkingMem 10G $THREADS)
        $QSUB -pe DJ $THREADS \
              -notify \
              -cwd \
              -l h_vmem=$MEM \
              -N "Kamboh-GP-c$CHR-p$PART" \
              -o $BASE/logs/*.GATK37.GP.chr$CHR.p$PART.log \
              -V \
              -j y \
              $BASE/GenotypeGVCF_tileDB-single.sh -v "$GVCF" -c "chr$CHR" -o $BASE/GSTEP-GATK37/*.hg38.tileDB.c$CHR-$PART.genotypedGVCF.g.vcf.gz -t $THREADS

        #break
  done
fi
