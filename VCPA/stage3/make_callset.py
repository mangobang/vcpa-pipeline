#!/usr/bin/env python2.7
#####
#Created: Jan-05-2018
#This is the python code to generate callsets.json file for running tileDB
#The callsets.json contains the information about the samples 


import argparse
import glob
import json
import os
from collections import OrderedDict

parser = argparse.ArgumentParser(description='Process a directory into a callset.json')

parser.add_argument('chr', help='chromosome filter')
parser.add_argument('directory', help='directory to search', nargs='+')
args = parser.parse_args()


fList=[]
for dir in args.directory:
  dirStr = dir + '/*/*.' + args.chr + '.g.vcf.gz'
  fList += glob.glob(dirStr)

fList.sort()
ct=0
c=dict()
for f in fList:
  d = dict(row_idx=ct, filename=f)
  b = os.path.basename(f).split(".")[0]
  c[b]=d
  ct+=1

# order by val:=row_idx, using OrderedDict()
print json.dumps(
                  dict(
                        callsets = OrderedDict(sorted(c.items(), key=lambda t: t[1]['row_idx']) )
                      ),
                  indent = 2,
                  sort_keys = False,
                  separators =(',', ': ')
                )


