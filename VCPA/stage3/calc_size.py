#!/usr/bin/env python2.7
#####
#Created: Jan-05-2018
#This is the python code to estimate the partitions that run within the make_loader.py

import pysam
import pandas as pd
from collections import OrderedDict
import json
import argparse

parser = argparse.ArgumentParser(description='Process a directory into a callset.json')

parser.add_argument('chr', help='chromosome filter')
parser.add_argument('cuts', help='number of partitions')
args = parser.parse_args()

chr=args.chr

# dict for chr start position along linear chr
cLookup = OrderedDict()
cLookup['chr1'] = 0
cLookup['chr2'] = 248956422
cLookup['chr3'] = 491149951
cLookup['chr4'] = 689445510
cLookup['chr5'] = 879660065
cLookup['chr6'] = 1061198324
cLookup['chr7'] = 1232004303
cLookup['chr8'] = 1391350276
cLookup['chr9'] = 1536488912
cLookup['chr10'] = 1674883629
cLookup['chr11'] = 1808681051
cLookup['chr12'] = 1943767673
cLookup['chr13'] = 2077042982
cLookup['chr14'] = 2191407310
cLookup['chr15'] = 2298451028
cLookup['chr16'] = 2400442217
cLookup['chr17'] = 2490780562
cLookup['chr18'] = 2574038003
cLookup['chr19'] = 2654411288
cLookup['chr20'] = 2713028904
cLookup['chr21'] = 2777473071
cLookup['chr22'] = 2824183054
cLookup['chrX'] =  2875001522
cLookup['chrY'] =  3031042417
cLookup['chrM'] =  3088269832

vcf = "*.chr%s.g.vcf.bgz" % chr # replace this gvcf file with one of your own gVCF

# Open VCF
bcf_in = pysam.TabixFile(vcf)

offset = cLookup['chr' + chr]
rd=[]

# store all snp start positions
for rec in bcf_in.fetch('chr' + chr):
  rd.append(int(rec.split()[1]) + offset)

# determine bin for approx equal quantity bins (quantiles)
g = pd.qcut(rd, int(args.cuts), retbins=True, labels=False)

# Convert results to int
f = map(int, g[1] )

rd=[]
ct=0
for idx in range(0,len(f) - 1):

  # end_pos is next interval
  if idx < len(f) - 2:
       end_pos = f[idx+1] - 1
  else:
      # on the final interval, end_pos is next chr in list
      i = cLookup.keys().index('chr' + chr)

      # for chrM, any number would terminate
      if i >= len(cLookup.keys()) -1:
         end_pos = 99999999999
      else:
         end_pos = cLookup.values()[i + 1] - 1

  g = dict(
         begin = f[idx],
         end   = end_pos,
         array="vd_chr%s_part%s" % (chr, idx),
         vcf_output_filename="/mnt/adsp/new_tileDB/pVCF_genomicsDB/CHR%s.part%s.g.vcf.gz" % (chr, idx)
          )
  rd.append(g)


print json.dumps(dict(column_partitions=rd),indent=2,
                  sort_keys=True,
                  separators=(',', ': ')
         )

