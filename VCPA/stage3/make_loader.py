#!/usr/bin/env python2.7
#####
#Created: Jan-05-2018
#This is the python code to generate loader.json file for running tileDB


from operator import itemgetter, attrgetter
import argparse
import glob
import json
import os
from subprocess import check_output

parser = argparse.ArgumentParser(description='Make a loader file from a chr#')

parser.add_argument('chr', help='chromosome')
parser.add_argument('parts', help='number of partitions')
args = parser.parse_args()


fp = open("loader.chr22.json")

c22 = json.load(fp)

newColsRaw = check_output(["./calc_size.py", args.chr, args.parts])

c22.update(json.loads(newColsRaw))
c22['callset_mapping_file'] = "/mnt/adsp/new_tileDB/callsets.json"

print json.dumps(c22, indent=2,
                  sort_keys=True,
                  separators=(',', ': ')
         )

