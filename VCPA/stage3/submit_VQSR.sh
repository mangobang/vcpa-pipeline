#!/bin/bash

source /mnt/adsp/pipeline.ini
source /mnt/adsp/lib/common_functions.sh

while getopts dh: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
   esac
done

QSUB=qsub
THREADS=16
BASE=/mnt/adsp/test_tileDB

    MEM=$(adjustWorkingMem 470G $THREADS)
    $QSUB -pe DJ $THREADS \
            -l h_vmem=$MEM \
            -N "*-VQSR-new.tileDB.all" \
            -o $BASE/new_logs/*.tileDB.new.all.log \
            -V \
            -cwd \
            -j y \
            $BASE/VQSR.sh -t $THREADS

