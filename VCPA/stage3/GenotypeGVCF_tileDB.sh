#!/bin/bash -x
#This code is for extention combined GVCF to do the genotype GVCF

source /mnt/adsp/pipeline.ini
source /mnt/adsp/lib/common_functions.sh

while getopts dnt:i:p:c:o: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      c) CHR="$OPTARG";;
      o) OUTFILE="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
   esac
done

VAR=""
for PAR in $PREFIX$CHR.*gz;do
    VAR+="--variant $PAR "
done

java -Djava.io.tmpdir=/mnt/adsp/results/VCF/pVCF/tmp -jar $GATK38 \
             -T GenotypeGVCFs \
             -R $REF_FASTA \
             --dbsnp $DBSNP \
             -nt $THREADS \
             -L chr$CHR \
             $VAR \
             -o "$OUTFILE"

