#!/bin/bash -x

source /mnt/adsp/pipeline.ini
source /mnt/adsp/lib/common_functions.sh

BASE=/mnt/adsp/test_tileDB
HIGH=$BASE/1000G_phase1.snps.high_confidence.hg38.vcf.gz

while getopts dnt:i:p:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      c) CHR="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
   esac
done

VAR=""
for CHR in {1..22} X Y M;do
    VAR+="--input $BASE/new_results/*.hg38.tileDB.recalibrate_SNP.chr$CHR.g.vcf.bgz "
done


java -Xmx430g -Djava.io.tmpdir=$TMP_DIR -jar $GATK \
             -T VariantRecalibrator \
             -R $REF_FASTA \
             -L chr1 -L chr2 -L chr3 -L chr4 -L chr5 \
             -L chr6 -L chr7 -L chr8 -L chr9 -L chr10 \
             -L chr11 -L chr12 -L chr13 -L chr14 -L chr15 \
             -L chr16 -L chr17 -L chr18 -L chr19 -L chr20 \
             -L chr21 -L chr22 -L chrX -L chrY -L chrM \
             --maxGaussians 4 \
             -resource:mills,known=false,training=true,truth=true,prior=12.0 $GOLD \
             -resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $DBSNP \
             -an QD \
             -an FS \
             -an DP \
             -an SOR \
             -an ReadPosRankSum \
             -an MQRankSum \
             -an InbreedingCoeff \
             -mode INDEL \
             -allPoly \
             -tranche 100.0 \
             -tranche 99.9 \
             -tranche 99.8 \
             -tranche 99.7 \
             -tranche 99.5 \
             -tranche 99.3 \
             -tranche 99.0 \
             -tranche 98.5 \
             -tranche 98.0 \
             -tranche 97.0 \
             -tranche 95.0 \
             -tranche 90.0 \
             $VAR \
             -nt $THREADS \
             -recalFile $BASE/new_results/*.hg38.tileDB.recalibrate_INDEL.recal \
             -tranchesFile $BASE/new_results/*.hg38.tileDB.recalibrate_INDEL.tranches \
             -rscriptFile $BASE/new_results/*.hg38.recal.tileDB.recalibrate_INDEL_plots.R

