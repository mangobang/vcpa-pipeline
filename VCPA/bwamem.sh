#!/bin/bash

#export DIR="/mnt/adsp/vcpa-pipeline"
#source $DIR/VCPA/stage0/pipeline.ini
#source $DIR/VCPA/stage0/common_functions.sh
#source $DIR/VCPA/stage0/seqfile_functions.sh

while getopts di: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLE="$OPTARG";;
   esac
done
 RGCT=$(samtools view -H $SAMPLE | grep -c "^@RG")
 if [ $RGCT -gt 1 ];then
   TOTMEM=$(free -g|grep Mem|awk '{print $2}')
 else
   TOTMEM=95
 fi
 MAX_THREADS=$(getconf _NPROCESSORS_ONLN)
 THREADS=$[ $MAX_THREADS / ($TOTMEM / 95) ]
 MEM=$(adjustWorkingMem 95G $THREADS)
 echo "${MEM}"
