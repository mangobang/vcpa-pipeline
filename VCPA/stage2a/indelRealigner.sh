#!/bin/bash
#####
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         GATK IndelRealigner
#####

# indelrealign.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-m TMP)       - tmp
# @param (-l L-chr)     - chrs
# @param (-c chr)       - chr

while getopts dn:i:p:r:b:m:l:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
      l) L="$OPTARG";;
      c) CHR="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item current_operation "IndelRealigner-${CHR}"


     java -Xmx12g -Djava.io.tmpdir=${TMP} \
          -jar $GATK \
          -T IndelRealigner\
          -R $REF_FASTA \
          -I ${INPUTBAM} \
          -known $KNOWN \
          -known $DBSNP \
          -known $GOLD \
          --targetIntervals ${PREFIX}.indels.intervals \
          -BQSR ${PREFIX}.recal_data.table \
          ${L} \
          --bam_compression 1 \
          -kpr \
          -SQQ 10 -SQQ 20 -SQQ 30 -SQQ 40 \
          --disable_indel_quals \
          --preserve_qscores_less_than 6 \
          -log $LOGS/indel.${CHR}.log \
          -o ${PREFIX}.indelrealign.${CHR}.bam

      status=$(grep "done" $LOGS/indel.${CHR}.log | wc -l)

      if [ $status == "1" ];then
         END=$(date +%s)
         TM=$[$END-$SRT];
         NOW=$(date '+%F+%T')
         echo $TM
         track_item current_operation "IndelRealigner-${CHR}-exit"

      else
         echo "IndelRealigner-ErrorState on ${CHR}" > $LOGS/indel.${CHR}.err.log
         error-to-hannah "IndelRealigner-ErrorState on ${CHR}"
         track_item current_operation "IndelRealigner-ErrorState on ${CHR}"
         exit 100
      fi
