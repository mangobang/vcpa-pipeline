#!/bin/bash
#####
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         Sambamba merge
#         Sambamba index
#####

# indelrealignmerge.sh
#
# @param (-i INPUTBAM)     - input BAM
# @param (-r RG)           - read-group for filtering by
# @param (-p PREFIX)       - out-file naming prefix
# @param (-b LIB)          - lib
# @param (-m TMP)          - tmp
# @param (-t THREADS)      - threads
# @param (-s MSTR)         - mstr
# @param (-c COMPRESSION)  - compression


while getopts dn:i:p:r:b:m:t:s:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
      t) THREADS="$OPTARG";;
      s) MSTR="$OPTARG";;
      c) COMPRESSION="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
source ${LIB}/seqfile_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)
NOW=$(date '+%F+%T')
SRT=$(date +%s)
track_item indelRealign_done_time $NOW
track_item current_operation "IndelRealigner-merge"

     #sumindels=$(grep "done" ${TMP}/indelrealign*.log | wc -l)
     #echo $sumindels
     #if [ $sumindels >= "27" ];then
       #rm -rf ${PREFIX}.sorted.dupmarked.bam
       #rm -rf ${PREFIX}.sorted.dupmarked.bam.bai
       echo ${MSTR}
       $SAMBAMBA merge --nthreads=${THREADS} --compression-level=${COMPRESSION} ${PREFIX}.hg38.realign.bqsr.bam ${MSTR}
       $SAMBAMBA index ${PREFIX}.hg38.realign.bqsr.bam
     #else
        #echo "Ingredients are not done"
        #track_item current_operation "IndelRealigner-NotDone"
     #fi

     #status=$(grep "sambamba-merge: Error" ${TMP}/indelrealignmerge.log | wc -l)

     #if [ $status == "0" ];then
        #rm -rf ${PREFIX}.indelrealign.*.bam
        #rm -rf ${PREFIX}.indelrealign.*.bam.bai
        #$SAMTOOLS view -C -T $REF_FASTA -@${THREADS} -o ${PREFIX}.hg38.realign.bqsr.cram ${PREFIX}.hg38.realign.bqsr.bam  #should just use sambamba (uses htslib, just like samtools)
        #$SAMBAMBA index -C ${PREFIX}.hg38.realign.bqsr.cram

        #END=$(date +%s)
        #TM=$[$END-$SRT]
        #NOW=$(date '+%F+%T')
        #echo "Completed in $TM secs, $NOW"
        #track_item indelRealign_merge_done_time $NOW
        #track_item indelRealign_merge_duration $TM
        #track_item indelRealign_bam_size $(stat -c '%s' ${PREFIX}.hg38.realign.bqsr.bam)
     #else
        #error-to-hannah "IndelRealigner-merge-ErrorState on $IID;$SM"
        #track_item current_operation "IndelRealigner-merge-ErrorState"
     #fi


[[ -z "$RESULTSPATH" ]] && export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)

if [ ! -z $RESULTSPATH ];then

    OUTFILE="${PREFIX}.hg38.realign.bqsr.bam"
    FNAME=$(basename "$OUTFILE")
    CRAMFILE=${OUTFILE/.bam/.cram}

    echo "Make md5"
    track_item current_operation "IndelRealigner-merge-MD5"
    md5sum $OUTFILE > $OUTFILE.md5
    #aws s3 cp $OUTFILE.md5 $RESULTSPATH/${FNAME/.bam/.md5}

    md5sum $CRAMFILE > $CRAMFILE.md5
    #aws s3 cp $CRAMFILE.md5 $RESULTSPATH/

    echo "Make flagstat"
    track_item current_operation "IndelRealigner-merge-flagstat"
    FLAGSTAT=${OUTFILE/.bam/.flagstat}
    $SAMBAMBA flagstat ${OUTFILE} > "$FLAGSTAT"
    #aws s3 cp "$FLAGSTAT" $RESULTSPATH/${FNAME/.bam/.flagstat}

    #echo "Begin upload"
    #track_item current_operation "IndelRealigner-merge-upload"
    #aws s3 cp --only-show-errors --storage-class STANDARD_IA  $OUTFILE $RESULTSPATH/$FNAME
    #aws s3 cp $OUTFILE.bai $RESULTSPATH/$FNAME.bai
    #aws s3 cp --only-show-errors --storage-class STANDARD_IA  $CRAMFILE $RESULTSPATH/
    #aws s3 cp $CRAMFILE.crai $RESULTSPATH/

    #if [ $(aws s3 ls $RESULTSPATH/$FNAME | grep -c .bam) == 0 ];then
    #   echoerr "bam not uploaded"
    #   error-to-hannah "Merge failed upload $SM"
    #   track_item current_operation "Merge-Upload-ErrorState"
    #   exit 100
    fi

    S3BAMPATH=$(rawurlencode "$RESULTSPATH/$FNAME")
    [[ ! -z "$S3BAMPATH" ]] && track_item bam_recal_path $S3BAMPATH

    track_item bqsr_rg_cnt $(readgroup_cnt $OUTFILE)
    track_item bqsr_bam_size $(stat -c '%s' $OUTFILE)
    track_item bqsr_bam_md5 $(cut -f1 $OUTFILE.md5)

    track_item bqsr_cram_size $(stat -c '%s' $CRAMFILE)
    track_item bqsr_cram_md5 $(cut -f1 $CRAMFILE.md5)

     # Extract flagstat stats
     TOT=$(grep "in total"     $FLAGSTAT | grep -Po "^\d+")
     DUPS=$(grep "duplicates"  $FLAGSTAT | grep -Po "^\d+")
     MAPPED=$(grep "mapped ("  $FLAGSTAT | grep -Po "^\d+")
     SINGLE=$(grep "singletons" $FLAGSTAT | grep -Po "^\d+")
     PAIRED=$(grep "properly paired" $FLAGSTAT | grep -Po "^\d+")

     DPCENT=$(echo $DUPS   | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     MPCENT=$(echo $MAPPED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     PPCENT=$(echo $PAIRED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     SPCENT=$(echo $SINGLE | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')

     # Upload flagstat stats
     track_item bqsr_ir_reads $TOT
     track_item bqsr_ir_dups $DUPS
     track_item bqsr_ir_pcent $DPCENT
     track_item bqsr_ir_mapped_reads $MAPPED
     track_item bqsr_ir_mapped_pcent $MPCENT
     track_item bqsr_ir_paired_reads $PAIRED
     track_item bqsr_ir_paired_pcent $PPCENT
     track_item bqsr_ir_singletons   $SINGLE
     track_item bqsr_ir_singletons_pcent $SPCENT
else
  echo "Missing RESULTSPATH, nothing uploaded"
fi
