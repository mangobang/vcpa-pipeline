#!/bin/bash


while getopts di: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLE="$OPTARG";;
   esac
done


 RGCT=$(samtools view -H $SAMPLE | grep -c "^@RG")
 if [ $RGCT -gt 1 ];then
   TOTMEM=$(free -g|grep Mem|awk '{print $2}')
 else
   TOTMEM=95
 fi
 MAX_THREADS=$(getconf _NPROCESSORS_ONLN)
 THREADS=$[ $MAX_THREADS / ($TOTMEM / 95) ]
 echo "${THREADS}"
