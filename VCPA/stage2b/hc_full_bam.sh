#!/bin/bash
#####
# Created: May-01-2017
# Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS)
#         GATK HaplotypeCaller
#####

# hc_full_bam.sh
#
# @param (-i INPUTBAM)     - input BAM
# @param (-r RG)           - read-group for filtering by
# @param (-p PREFIX)       - out-file naming prefix
# @param (-b LIB)          - lib
# @param (-m TMP)          - tmp
# @param (-t THREADS)      - threads
# @param (-l CHR)          - chr


while getopts dn:i:p:r:b:m:t:l: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
      t) THREADS="$OPTARG";;
      l) CHR="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
track_item current_operation "HaplotypeCaller-${CHR}"
     # set default PCRMODEL to CONSERVATIVE, overriden by getopts
     java -Djava.io.tmpdir=${TMP} \
          -jar $GATK \
          -T HaplotypeCaller \
          -R $REF_FASTA \
          -I ${INPUTBAM} \
          -nct ${THREADS} \
          -L ${CHR} \
          --dbsnp $DBSNP \
          --genotyping_mode DISCOVERY \
          --minPruning 2 \
          -newQual \
          -stand_call_conf 30 \
          --emitRefConfidence GVCF \
          --pcr_indel_model "CONSERVATIVE" \
          -l INFO \
          -log $LOGS/vch.${CHR}.log \
          -o "${PREFIX}.${CHR}.g.vcf.gz"


status=$(grep "done" $LOGS/vch.${CHR}.log | wc -l)

if [ $status == "0" ];then
   sleep 60
        java  -Djava.io.tmpdir=${TMP} \
          -jar $GATK \
          -T HaplotypeCaller \
          -R $REF_FASTA \
          -I ${INPUTBAM} \
          -nct ${THREADS} \
          -L ${CHR} \
          --dbsnp $DBSNP \
          --genotyping_mode DISCOVERY \
          --minPruning 2 \
          -newQual \
          -stand_call_conf 30 \
          --emitRefConfidence GVCF \
          --pcr_indel_model "CONSERVATIVE" \
          -l INFO \
          -log $LOGS/vch.${CHR}.log \
          -o "${PREFIX}.${CHR}.g.vcf.gz"

fi


if [ ! -s "${PREFIX}.${CHR}.g.vcf.gz" ];then
  echo "HaplotypeCaller-ErrorState on ${CHR}" > ${TMP}/logs/vch.${CHR}.err.log
  error-to-hannah "HaplotypeCaller-ErrorState on $IID;$SM"
  track_item current_operation "HaplotypeCaller-ErrorState"
else
  track_item current_operation "HaplotypeCaller-${CHR}-exit"

fi
