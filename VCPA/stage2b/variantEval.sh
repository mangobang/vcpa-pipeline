#!/bin/bash
#####
# Created: May-01-2017
# Stage2b Performing individual genotype call, generate gVCFs (GATK) and VCFs (ATLAS)
#         GATK VariantEval
#####

# variantEval.sh
#
# @param (-i INPUTVCF)     - input VCF
# @param (-p PREFIX)       - out-file naming prefix
# @param (-b LIB)          - lib
# @param (-l Logs)         - logs
# @param (-m TMP)          - tmp
# @param (-t THREADS)      - threads


while getopts dn:i:p:b:l:m:t: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTVCF="$OPTARG";;
      p) PREFIX="$OPTARG";;
      b) LIB="$OPTARG";;
      l) L="$OPTARG";;
      m) TMP="$OPTARG";;
      t) THREADS="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)

SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item variant_eval_st_tm $NOW
track_item current_operation "VariantEval"

     # set default PCRMODEL to CONSERVATIVE, overriden by getopts
     java -Djava.io.tmpdir=${TMP} \
          -jar $GATK \
          -T VariantEval \
          -R $REF_FASTA \
          -nt ${THREADS} \
          --dbsnp $DBSNP \
          --goldStandard $GOLD \
          -EV CountVariants \
          -EV VariantSummary \
          ${INPUTVCF} \
          --mergeEvals \
          -o "${PREFIX}.eval.txt"

if [ ! -s "${PREFIX}.eval.txt" ];then
  track_item current_operation "VariantEval-ErrorState"
else
  track_item current_operation "Complete"
  END=$(date +%s)
  TM=$[$END-$SRT]

  NOW=$(date '+%F+%T')
  track_item variant_eval_dn_tm $NOW
  track_item variant_eval_duration $TM

fi

INFILE="${PREFIX}.eval.txt"
NSNPS=$(grep "VariantSummary.*all" $INFILE | awk '{print $8}' )
track_item variantEval_nSNPs $NSNPS

TITV=$(grep "VariantSummary.*all" $INFILE | awk '{print $9}' )
track_item variantEval_TiTv $TITV

NINDEL=$(grep "VariantSummary.*all" $INFILE | awk '{print $14}' )
track_item variantEval_nIndel $NINDEL

TOTAL_SITES=$(grep "CountVariants.*all " $INFILE | awk '{print $6}' )
track_item variantEval_totSites $TOTAL_SITES

TOTAL_VARIANTS=$(grep "CountVariants.*all " $INFILE | awk '{print $9}' )
track_item variantEval_totVariants $TOTAL_VARIANTS

KNOWN_SITES=$(grep "CountVariants.*known " $INFILE | awk '{print $9}' )
track_item variantEval_knSites $KNOWN_SITES


RATIO1=$(echo $KNOWN_SITES|awk -v tv=$TOTAL_VARIANTS '{print (1-($1/tv))*100}')
track_item novel_over_total_variant_perc $RATIO1

TITV_K=$(grep "TiTvVariantEvaluator.*known" $INFILE | awk '{print $8}')
track_item variantEval_known_TiTv $TITV_K

TITV_N=$(grep "TiTvVariantEvaluator.*novel " $INFILE | awk '{print $8}')
track_item variantEval_novel_TiTv $TITV_N


NTI_T=$(grep "TiTvVariantEvaluator.*all" $INFILE | awk '{print $6}')
track_item variantEval_total_nTi $NTI_T

NTI_K=$(grep "TiTvVariantEvaluator.*known" $INFILE | awk '{print $6}')
track_item variantEval_known_nTi $NTI_K

NTI_N=$(grep "TiTvVariantEvaluator.*novel " $INFILE | awk '{print $6}')
track_item variantEval_novel_nTi $NTI_N


NTV_T=$(grep "TiTvVariantEvaluator.*all" $INFILE | awk '{print $7}')
track_item variantEval_total_nTv $NTV_T

NTV_K=$(grep "TiTvVariantEvaluator.*known " $INFILE | awk '{print $7}')
track_item variantEval_known_nTv $NTV_K

NTV_N=$(grep "TiTvVariantEvaluator.*novel " $INFILE | awk '{print $7}')
track_item variantEval_novel_nTv $NTV_N

  if [ -z $RESULTSPATH ];then
     export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
  fi

  if [ ! -z $RESULTSPATH ];then
     echo "uploading eval file"
     find $LOGS -size 0 -delete # delete empty files

     F=$(basename $INFILE)
     #aws s3 cp "$INFILE" "$RESULTSPATH/VCF/GATK/$F"

     echo "uploading pipeline logs"
     for FLOG in $LOGS/*.lo*;do
       V=$(basename $FLOG)
       #aws s3 cp "$FLOG" "$RESULTSPATH/LOGS/$V"
     done

     for LOGS in ${L}/*.lo*;do
       V=$(basename $LOGS)
       #aws s3 cp "$LOGS" "$RESULTSPATH/LOGS/$V"
     done

  else
    echoerr "Not uploading, no \$RESULTSPATH"
    error-to-hannah "VariantEval-upload-ErrorState on $IID;$SM"
    track_item current_operation "VariantEval-upload-ErrorState"
    exit 100
  fi

  FILECOUNT=$(aws s3 ls --recursive $RESULTSPATH | wc -l)
  track_item current_operation "completed-$FILECOUNT-files"
