#!/bin/bash
#
# markCompletedHC.sh
# @param (-i INPUTBAM)     - input BAM
# @param (-p PREFIX)       - out-file naming prefix
# @param (-b LIB)          - lib
# @param (-m TMP)          - tmp


while getopts dn:i:p:r:b:m:t:l: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
   esac
done

source ${LIB}/pipeline.ini
source ${LIB}/common_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)

track_item current_operation "HaplotypeCaller-stats-upload"

READS=$(awk 'BEGIN{SM=0};{ if (/total reads/){SM=SM+$16}} END{print SM;}' ${TMP}/hc-*.log)
track_item gatk_hc_processed_reads $READS

FILTERPERC=$(perl -nle 'if ( $_ =~ /(\d+) reads were filtered out during the traversal/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_total_filter_reads $FILTERPERC

CIGARFILTER=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing BadCigarFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_BadCigarFilter_reads $CIGARFILTER

DUPFILTER=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing DuplicateReadFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_DuplicateReadFilter_reads $DUPFILTER

FVQCFILTER=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing FailsVendorQualityCheckFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_FailsVendorQualityCheckFilter_reads $FVQCFILTER

VAR1=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing HCMappingQualityFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_HCMappingQualityFilter_reads $VAR1

VAR2=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing MalformedReadFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_MalformedReadFilter_reads $VAR2

VAR3=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing MappingQualityUnavailableFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_MappingQualityUnavailableFilter_reads $VAR3

VAR4=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing NotPrimaryAlignmentFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_NotPrimaryAlignmentFilter_reads $VAR4

VAR5=$(perl -nle 'if ( $_ =~ /(\d+) reads .*? failing UnmappedReadFilter/){$sum+=$1;} } END { print $sum' ${TMP}/hc-*.log)
track_item gatk_hc_UnmappedReadFilter_reads $VAR5

NOW=$(date '+%F+%T');
  track_item gatk_hc_done_tm $NOW

  if [ -z $RESULTSPATH ];then
     export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
  fi

  if [ ! -z $RESULTSPATH ];then
     echo "uploading VCFs"

     find $(dirname ${PREFIX}.${CHR}.g.vcf.gz) -size 0 -delete # delete empty files

     for GZ in ${PREFIX}*.gz*;do
        F=$(basename $GZ)
        #aws s3 cp --storage-class STANDARD_IA "$GZ" "$RESULTSPATH/VCF/GATK/$F"
     done

     CT=$(ls ${PREFIX}*.g.vcf.gz | wc -l)
     track_item gatk_hc_completed_count "$CT"
     SUMSNP=$(zgrep -hc "^[^#]" ${PREFIX}*.g.vcf.gz|awk 'BEGIN{SM=0;}{SM=$1+SM;}END{print SM;}')

     [[ ! -z $SUMSNP ]] && track_item gatk_hc_total_variants $SUMSNP

     echo "uploading GATK HC logs"
     for LG in ${LOGS}/hc*.log;do
       V=$(basename $LG)
       #aws s3 cp "$LG" "$RESULTSPATH/VCF/GATK/"
     done
     track_item stage2b_GATK_logs_path $(rawurlencode "$RESULTSPATH/VCF/GATK/")


     if [ "$CT" -ne 25 ];then
       echoerr "Too Few VCFs"
       track_item current_operation "HC-VCF-COUNT-ErrorState"
       exit 100
     fi

  else
    echoerr "Not uploading, no \$RESULTSPATH"
    track_item current_operation "HC-upload-ErrorState"
    exit 100
  fi
