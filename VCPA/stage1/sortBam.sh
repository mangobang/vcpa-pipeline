#!/bin/bash
#####
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)
#         Samtools sort
#####

# sortBam.sh
#
# @param (-i INPUTSAM)  - input SAM
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-l CHR)       - chr
# @param (-m TMP)       - tmp

while getopts dn:i:p:r:b:l:m: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTSAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      l) CHR="$OPTARG";;
      m) TMP="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
NOW=$(date '+%F+%T')
track_item sortsam_start_tm $NOW
track_item current_operation "SortSam"

# sort input SAM.GZ by name as required input for samblaster, output to co sorted BAM
$SAMTOOLS sort --threads 2 -l 0 -n -T "${PREFIX}/${RG}.aligned.${CHR}.sorted.bam.1" --output-fmt SAM "${INPUTSAM}" | \
  $SAMBLASTER --addMateTags --ignoreUnmated  | \
    $SAMTOOLS sort -@2     -l 5    -T "${PREFIX}/${RG}.aligned.${CHR}.sorted.bam.2" --output-fmt BAM -o "${PREFIX}/${RG}.aligned.${CHR}.sorted.bam"

rm -rf "${INPUTSAM}"
rm -rf "${PREFIX}/${RG}.aligned.${CHR}.sorted.bam.1"
rm -rf "${PREFIX}/${RG}.aligned.${CHR}.sorted.bam.2"

if [ ! -s "${PREFIX}/${RG}.aligned.${CHR}.sorted.bam" ];then
  track_item current_operation "SortSam-ErrorState"

fi

NOW=$(date '+%F+%T')
track_item sortsam_done_tm $NOW
