#!/bin/bash
#####
# Created: May-01-2017
# Stage1  markdepthOfCoverage for API
#####

# markCompletedDepthOfCoverage.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-p PREFIX)    - out-file naming prefix

while getopts dn:i:p:b: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      b) LIB="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
source ${LIB}/seqfile_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)


INFILE=${PREFIX}.depth.txt
RSIZE=$(get_read_size ${INPUTBAM})
ATGC_refSize=2745186602 #hg38 - w/o N's
track_item current_operation "DepthOfCoverage-stats-upload"
# average depth: number of reads covering an interval base, where 1.0 is one read covers each interval base on average
# base depth count divided by length of all intervals,
AVGDEPTH=$(awk '{ split($1,rng,"-");split(rng[1],a,":");TLEN+=rng[2] - a[2];DEPTH+=$2;}END{print DEPTH/TLEN;}' $INFILE)

# extract % from .sample_summary columns
track_item depth_5x $(tail -2 $INFILE|head -1|cut -f7)
track_item depth_10x $(tail -2 $INFILE|head -1|cut -f8)
track_item depth_20x $(tail -2 $INFILE|head -1|cut -f9)
track_item depth_30x $(tail -2 $INFILE|head -1|cut -f10)
track_item depth_40x $(tail -2 $INFILE|head -1|cut -f11)
track_item depth_50x $(tail -2 $INFILE|head -1|cut -f12)


export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
echo "RESULTSPATH: " $RESULTSPATH

if [ ! -z $RESULTSPATH ];then
    echo "uploading depth of coverage file"
     F=$(basename $INFILE)
     #aws s3 cp "$INFILE" "$RESULTSPATH/$F"

     MEM=$(free -b | grep Mem | awk '{printf "%0.f", $2 * 1.88}')
     qconf -rattr exechost complex_values h_vmem=$MEM `hostname -f`
else
   echoerr "Not uploading, no \$RESULTSPATH"
   track_item current_operation "DepthOfCoverage-upload-ErrorState"
fi
