#!/bin/bash
#####
# Created: May-01-2017
# Stage1  Check data-quality of mapping (pre-VCF check)
#         Sambamba depth
#####

# depthOfCoverage.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-l CHR)       - chromosome
# @param (-p PREFIX)    - out-file naming prefix

while getopts dn:i:p:l:b: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      l) TARGET="$OPTARG";;
      b) LIB="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)
SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item cal_depth_start_tm $NOW
track_item current_operation "DepthOfCoverage-${CHR}"


      #  count depthOfCoverage between 5x, 10x, 20x, 30x, 40x, 50x coverage

      java -Xmx4g -jar $GATK \
           -T DepthOfCoverage \
           -R $REF_FASTA \
           --minMappingQuality 1 \
           --omitDepthOutputAtEachBase \
           --omitLocusTable \
           -ct 5 -ct 10 -ct 20 -ct 30 -ct 40 -ct 50 \
           -I ${INPUTBAM} \
           -L ${TARGET} \
           -o ${PREFIX}.depth.txt



END=$(date +%s)
TM=$[$END-$SRT];
NOW=$(date '+%F+%T')
echo $TM

       if [ ! -s "${PREFIX}.depth.txt" ];then
          track_item current_operation "DepthOfCoverage-ErrorState"
       else
          track_item cal_depth_done_time $NOW

       fi
