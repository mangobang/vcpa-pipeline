#!/bin/bash
#
# sambambareadname_sort.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-t THREADS)   - number of thread
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib


while getopts dnt:i:p:r:b: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
NOW=$(date '+%F+%T');
SRT=$(date +%s)
track_item rg_sort_start_tm $NOW
track_item current_operation "readname-sort"

if [ ${RG} == "A" ] || [ ${RG} == "1" ];then
  FILTER=""
else
  FILTER="--filter [RG]=="\'${RG}\'
fi

     $SAMBAMBA sort \
                 --tmpdir=$LOGS/${RG}.sambambarsort.log  \
                 --nthreads ${THREADS} \
                 --sort-by-name \
                 --compression-level 8 \
                 $FILTER \
                 -o  ${PREFIX}/${RG}.sorted-byname.bam ${INPUTBAM}
                
if [ ! -s "${PREFIX}/${RG}.sorted-byname.bam" ];then
  track_item current_operation "Missing ${RG}.sorted-byname.bam"
  
fi

END=$(date +%s)
TM=$[$END-$SRT]

NOW=$(date '+%F+%T');
track_item rg_sort_end_tm $NOW
track_item rg_sort_duration $TM