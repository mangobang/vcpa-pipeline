##
# echoerr - print msg to stderr
##
function echoerr() { echo "$@" 1>&2; }
####
# detect_hostname - grab hostname into HOST so that
# we can grab strings. Skip if hostname is of 'ip-xxxx...' form and retry
####
function detect_hostname(){
  export HOST=$(hostname -f)
  CT=0
  while [[ "$HOST" == ip* ]];do

    # get starcluster security group which has assigned hostname
    export HOST=$(curl -sS http://169.254.169.254/latest/meta-data/security-groups|sed 's/^@sc-//')

    CT=$[ $CT + 1 ]
    if [ $CT -gt 9 ];then
      get_instance_id #provides $IID
      export HOST=$(aws ec2 describe-tags --filters  Name=resource-type,Values=instance Name=resource-id,Values=$IID Name=key,Values=Name | grep -Po "${HOST_PREFIX}[^\"]+")
    fi
  done
  echo -n "$HOST"
}

function init_run(){
 if [ "$ENABLE_DB" != "1" ];then
   echo -n 0
   return
 fi

 if [ -z $RID ];then
  local PRJ_ID=$1
  local SM=$2
  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$

  if [ ! -z "$API_HOST" ] && [ ! -z "$PRJ_ID" ];then
      local RUNID=$(curl -sS "$API_HOST/v1/track/init-run/$PRJ_ID/$SM" | grep -Po 'id":"[^"]+"' | sed 's/id":"\|"//g')
      local PG=$(curl -sS "$API_HOST/v1/sample/set-attr/latest_run_id/$PRJ_ID/$SM/$RUNID")
      echo -n $RUNID
  else
      echoerr "Missing \$API_HOST:$API_HOST \$PRJ_ID:PRJ_ID"
  fi
 else
   echo -n $RID
 fi
}

function get_run(){
  if [ "$ENABLE_DB" != "1" ];then
    echo -n 0
    return 0
  fi

  if [ ! -z $RID ];then
    echo -n $RID
    return 0
  fi

  local PRJ_ID=$(get_project_id)
  local SM=$(get_sample_name)

  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$

  echo -n $(curl -sS "$API_HOST/v1/track/get-run/$PRJ_ID/$SM" | grep -Po 'id":"[^"]+"' | sed 's/id":"\|"//g')
}

function common_vars(){
  local HOST=$(detect_hostname)

  if [ -z $HOST ];then
    echoerr "Error: Missing HOST! Can not continue"
    error-to-otto "Missing HOST! Can not continue"
    kill -INT $$
  else
    export PRJ_ID=$(echo $HOST|grep -Po "P\d+"|sed 's/P//')
    export SM=$(echo $HOST|grep -Po "P\d+\S+"|sed 's/P[0-9]\+-//'|sed 's/-master//')
  fi

  echo "PRJ_ID: $PRJ_ID"
  echo "SM: $SM"
}


function get_sample_name(){
  if [ "$ENABLE_DB" != "1" ];then
    echo -n 0
    return
  fi

  if [ ! -z $SM ];then
    echo -n $SM
    return 0
  fi

  local HOST=$(detect_hostname)

  if [ -z $HOST ];then
    echoerr "Error: Missing HOST! Can not continue"
    error-to-otto "Missing HOST! Can not continue"
    kill -INT $$
  else
    local H=$(echo $HOST|grep -Po "P\d+\S+"|sed 's/P[0-9]\+-//'|sed 's/-master//')
    # make exception for ADNI naming error
    if [[ "$H" == ADNI-* ]];then
      H=$(echo $H|sed 's/-/_/g')
    elif [[ "$H" == LP* ]];then
      H=$(echo $H|sed 's/DNA-/DNA_/g')
    fi
    echo -n $H
  fi
}
function get_project_id(){
  if [ "$ENABLE_DB" != "1" ];then
   echo -n 0
   return
  fi

  if [ ! -z $PRJ_ID ];then
   echo -n $PRJ_ID
   return 0
  fi

  local HOST=$(detect_hostname)

  if [ -z $HOST ];then
    echoerr "Error: Missing HOST! Can not continue"
    error-to-otto "Missing HOST! Can not continue"
    kill -INT $$
  else
    echo -n $(echo $HOST|grep -m 1 -Po  "\-P\d+" | head -1 | sed 's/-P//')
  fi
}

function get_project_center(){
 if [ "$ENABLE_DB" != "1" ];then
   echo -n 0
   return
 fi

 if [ -z $PRJ_ID ];then
   export PRJ_ID=$(get_project_id)
 fi
 if [ ! -z "$API_HOST" ] && [ ! -z "$PRJ_ID" ];then
   echo -n $(curl -sS --get -d project_id=$PRJ_ID "$API_HOST/v1/projects/get-attr/project_center" | grep -Po 'value":"[^"]+"' | sed 's/value":"\|"//g')
 else
   echoerr "Missing \$API_HOST:$API_HOST \$PRJ_ID:PRJ_ID"
 fi
}
function get_subproject(){
 if [ "$ENABLE_DB" != "1" ];then
   echo -n 0
   return
 fi

 if [ -z $PRJ_ID ];then
   export PRJ_ID=$(get_project_id)
 fi
 if [ ! -z "$API_HOST" ] && [ ! -z "$PRJ_ID" ];then
   echo -n $(curl -sS --get -d project_id=$PRJ_ID "$API_HOST/v1/projects/get-attr/subproject_name" | grep -Po 'value":"[^"]+"' | sed 's/value":"\|"//g')
 fi
}

function setup_dirs(){
mkdir -vp $BAM_DIR
mkdir -vp $RESULTS_DIR

mkdir -vp $RESULTS_DIR/tmp
mkdir -vp $RESULTS_DIR/bam
mkdir -vp $BAM_DIR/log
mkdir -vp $DIR/logs
}

function get_instance_id(){
  export IID=$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)
}
function get_instance_type(){
  echo -n $(curl -sS http://169.254.169.254/latest/meta-data/instance-type)
}

function get_instance_ami(){
  echo -n $(curl -sS http://169.254.169.254/latest/meta-data/ami-id)
}


####
# get_vol_id - grab the EBS volume-id
####
function get_vol_id(){
  export VOL=$(aws ec2 describe-instance-attribute --instance-id $IID --attribute blockDeviceMapping|grep -Po "vol-\w+")
}

function get_zone(){
  echo -n $(curl -sS http://169.254.169.254/latest/meta-data/placement/availability-zone/)
}

####
# attach tags for aws cost tracking
####
function tag_instance(){
 export PROJECT=$(get_project_center)
 local SUBPROJECT=$(get_subproject)
 get_instance_id
 get_vol_id

 if [ -z $PROJECT ];then
  echoerr "Error: Missing PROJECT tag variable. Exiting"
  kill -INT $$
 fi

 /usr/bin/aws \
   ec2 create-tags \
   --resources $IID $VOL \
   --tags Key=COSTS-HOST,Value=$SM \
          Key=PROJECT,Value=$PROJECT \
          Key=SUBPROJECT,Value=$SUBPROJECT

}
function get_s3_seqfile_location(){
  if [ "$ENABLE_DB" != "1" ];then
   return
  fi

  local PRJ_ID=$1
  local SM=$2
  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$

  local S3PATH=$(curl -sS "$API_HOST/v1/sample/get-attr/seqfile_s3_uri/$PRJ_ID/$SM"|grep -Po 'value":"[^"]+'|sed 's/value":"//')
  if [ ! -z "$S3PATH" ];then
    echo -n $(rawurldecode $S3PATH)
  else
    echoerr "Empty \$S3PATH"
  fi
}

function get_s3_results_location(){
  if [ "$ENABLE_DB" != "1" ];then
   return
  fi

  local PRJ_ID=$1
  local SM=$2
  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$
  local RID=$(get_run)

  local S3PATH=$(curl -sS "$API_HOST/v1/sample/get-attr/results_s3_uri/$PRJ_ID/$SM"|grep -Po 'value":"[^"]+'|sed 's/value":"//'|sed 's#/$##')
  if [ ! -z "$S3PATH" ];then
    local URI=$(rawurldecode $S3PATH)
    URI=$(echo $URI|sed "s/_RID_/$RID/")
    echo -n $URI
  else
    echoerr "Empty \$S3PATH"
  fi
}

function get_sample_attr(){
  if [ "$ENABLE_DB" != "1" ];then
   return
  fi

  local ATTR=$1
  if [ -z "$2" ];then
     local PRJ_ID=$(get_project_id)
  else
     local PRJ_ID=$2
  fi
  if [ -z "$3" ];then
     local SM=$(get_sample_name)
  else
     local SM=$3
  fi

  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$

  echo -n $(curl -sS "$API_HOST/v1/sample/get-attr/$ATTR/$PRJ_ID/$SM"|grep -Po 'value":"[^"]+'|sed 's/value":"//')
}

###
# Send message to Otto via Pushover API
###
function error-to-otto(){
  if [ "$ENABLE_PUSHOVER_API" != "1" ];then
    return
  fi
  get_instance_id
  detect_hostname
  MSG="$1 $IID"
  curl -s \
    -d "token=ACa3OIDl7fTKbPPqLr0zdwvQbljEkj" \
    -d "user=ieIUwbVRFhkJIbDW2WZzGZBMdz9seJ" \
    -d "message=$MSG : $HOST" \
       https://api.pushover.net/1/messages.json

}

function mark_2a_start(){
 NOW=$(date "+%F+%T")
 if [ ! -z "$URI" ];then
   curl -s "$URI/$SM/$PRJ_ID/aws_processing_start_tm_stage2a/$NOW"
   curl -s "$URI/$SM/$PRJ_ID/aws_is_processing_stage2a/1"
 fi
}

function mark_2a_end(){
 NOW=$(date "+%F+%T")
 if [ ! -z "$URI" ];then
   curl -s "$URI/$SM/$PRJ_ID/aws_processing_end_tm_stage2a/$NOW"
   curl -s "$URI/$SM/$PRJ_ID/aws_is_processing_stage2a/0"
 fi
}
function mark_2b_start(){
 NOW=$(date "+%F+%T")
 if [ ! -z "$URI" ];then
   curl -s "$URI/$SM/$PRJ_ID/aws_processing_start_tm_stage2b/$NOW"
   curl -s "$URI/$SM/$PRJ_ID/aws_is_processing_stage2b/1"
 fi
}

function mark_2b_end(){
 NOW=$(date "+%F+%T")
 if [ ! -z "$URI" ];then
   curl -s "$URI/$SM/$PRJ_ID/aws_processing_end_tm_stage2b/$NOW"
   curl -s "$URI/$SM/$PRJ_ID/aws_is_processing_stage2b/0"
 fi
}

#
# wait for shutdown based on number of jobs in queue
# when number is zero, use *dd* to zero the file, then shutdown instance
function process_shutdown(){

  while true;do
      sleep 300
      if [ $(qstat|grep -Pc "^\s+\d") -eq 0 ];then
         for BAM in $BAM_DIR/*.bam;do
           export SM=$(basename $BAM | sed s'/\.bam//')
           dd if=/dev/zero of=$BAM count=20000000
           NOW=$(date "+%F+%T")
           track_item aws_processing_end_tm_stage2b $NOW
           track_item instance_end_tm $NOW
           track_item aws_is_processing_stage2a 0
           track_item aws_is_processing_stage2b 0
           track_item current_operation "completion"

         done

         /usr/bin/aws ec2 terminate-instances --instance-ids $IID
      fi
  done;

}

function instant_shutdown(){
get_instance_id
error-to-otto "instance shutdown"
aws s3 cp /var/log/cloud-init-output.log ${S3_LOG_DIR}/cloud-init-output.$IID.log
track_item current_operation "instant-shutdown"
/usr/bin/aws ec2 terminate-instances --instance-ids $IID
}

###############
# adjustWorkingMem - calculate the per-thread memory for a parallel qsub job
# @param TARGET - The total memory required by the job
# @param THREADS - Number of slots used by the job
# @return string amount of megabytes with M suffix
function adjustWorkingMem {

local TARGET=$1
local THREADS=$2
local MEM_REQ=1M

# test empty result
if [ ! -z $(echo $TARGET|grep -o G) ]
 then
   # contains G (Gigabytes)
   MBTARGET=$(echo $TARGET|grep -Po "[\d\.]+")

   # Convert to MB
   MBTARGET=$(echo $MBTARGET | awk '{print $1 * 1024}')
   #echo "New target:= $MBTARGET;"

   MEM_REQ=$(echo $MBTARGET | awk -v THREADS=$THREADS '{print $1 / THREADS }')

elif [ ! -z $(echo $TARGET|grep -o M) ]
 then
   # contains M (Megabytes)
   MBTARGET=$(echo $TARGET|grep -Po "[\d\.]+")

   # MEM_REQ=$[ $MBTARGET / $THREADS ]
   MEM_REQ=$(echo $THREADS | awk -v MBTARGET=$MBTARGET '{print MBTARGET / $1 }')

else
   MEM_REQ=$[ $TARGET / 1024 / 1024 / $THREADS ]
fi

if [ "$MEM_REQ" == "0" ]
 then
   MEM_REQ=1M
else
   MEM_REQ=${MEM_REQ}M
fi

 echo "$MEM_REQ"
}

# @param $1 - urlencoded string
# @return - decoded string
#
function rawurldecode
{
    printf $(printf '%b' "${1//%/\\x}")
}

#
# twice url encoding for web urls
# @param $1 - string to be encoded
# @return - urlencoded *2 string
#
function rawurlencode(){
  echo -n "$1" | perl -pe 's/([^a-zA-Z0-9_.!~*()'\''-])/sprintf("%%%02X", ord($1))/ge' | perl -pe 's/(\W)/sprintf("%%%02X", ord($1))/ge'
#  echo -n $URI | od -An -tx1 | tr ' ' % | xargs printf "%s"
}

#
# @param TRACK_NAME - the tracking attribute name
# @param VAL - the attribute value
# @return - web result details as JSON
#
function track_item(){
 if [ "$ENABLE_DB" != "1" ];then
   return
 fi

 local TRACK_NAME="$1"
 local VAL="$2"
 local RID=$(get_run)

 [[ -z "$RID" ]] && echoerr "Missing \$RID" && kill -INT $$

 echo $(curl -sS "$URI/$RID/$TRACK_NAME/$VAL")

}

function make_ebs_drive(){
    local BASE_SIZE="$1"
    local MULTIPLYER="$2"

    local VT="gp2"
    local VT_MAX=16384
    local ZONE=$(get_zone)

    local DEVNAME="/dev/xvdba"
    local PV=$DEVNAME
    get_instance_id #$IID

    local SZ1=$(echo $BASE_SIZE|awk -v ML=$MULTIPLYER '{printf "%3.0f", $1/1024^3 * ML}')

    # grab and attach another EBS if the required size is greater then VT_MAX
    if [ $SZ1 -gt $VT_MAX ];then
         SZ2=$[ $SZ1 - $VT_MAX ]
         SZ1=$VT_MAX
         local VOL2=$(aws ec2 create-volume --availability-zone $ZONE --size $SZ2 --volume-type $VT|grep -Po 'vol-[^"]+')

         sleep 20 && aws ec2 attach-volume --volume-id $VOL2 --device /dev/xvdbb --instance-id $IID
         sleep 20 && aws ec2 modify-instance-attribute --instance-id $IID --block-device-mappings "[{\"DeviceName\":\"/dev/xvdbb\",\"Ebs\":{\"DeleteOnTermination\":true}}]"
         PV="$DEVNAME /dev/xvdbb"
    fi

    local VOL1=$(aws ec2 create-volume --availability-zone $ZONE --size $SZ1 --volume-type $VT|grep -Po 'vol-[^"]+')

    #wait for volume available
    STATE=$(aws --output text ec2 describe-volumes --volume-ids $VOL1 --query Volumes[].State)
    while [ $STATE != "available" ];do sleep 1;STATE=$(aws --output text ec2 describe-volumes --volume-ids $VOL1 --query Volumes[].State);done;

    aws ec2 attach-volume --volume-id $VOL1 --device $DEVNAME --instance-id $IID && \
    sleep 10 && aws ec2 modify-instance-attribute --instance-id $IID --block-device-mappings "[{\"DeviceName\":\"$DEVNAME\",\"Ebs\":{\"DeleteOnTermination\":true}}]" && \
    pvcreate $PV && \
    vgcreate vg_main $PV && \
    lvcreate -l 100%VG --wipesignatures y --name lv_main vg_main

    if [ -e /dev/vg_main/lv_main ];then
       mkfs.xfs -f /dev/vg_main/lv_main && \
       mkdir -p /mnt/adsp && \
       mount -v /dev/vg_main/lv_main /mnt/adsp

    fi

    tag_instance



}

#
# create EBS drive, attach and lvm mount for TMP_DIR space
# not used in instance types d2.*, i3.*, r4.*
# if requested size is larger than 4.9 * bam_size then request and join another EBS
# @return TMP_DIR
#
function make_new_tmp(){
  if [ ! -e /dev/xvdba ] && \
      [[ `echo -n $(get_instance_type)` != d2* ]] && \
      [[ `echo -n $(get_instance_type)` != i3* ]] && \
      [[ `echo -n $(get_instance_type)` != r4* ]] && \
      [[ -z "$WES" ]];then


    local VT="gp2"
    local VT_MAX=16384
    local ZONE=$(get_zone)
    local SZ1=$(ls -lh $BAM_DIR/$SM.bam|awk '{printf "%3.0f", $5 * 4.9}')
    local DEVNAME="/dev/xvdba"
    local PV=$DEVNAME
    get_instance_id #$IID

    # grab and attach another EBS if the required size is greater then VT_MAX
    if [ $SZ1 -gt $VT_MAX ];then
         SZ2=$[ $SZ1 - $VT_MAX ]
         SZ1=$VT_MAX
         local VOL2=$(aws ec2 create-volume --availability-zone $ZONE --size $SZ2 --volume-type $VT|grep -Po 'vol-[^"]+')

         sleep 25 && aws ec2 attach-volume --volume-id $VOL2 --device /dev/xvdbb --instance-id $IID
         sleep 10 && aws ec2 modify-instance-attribute --instance-id $IID --block-device-mappings "[{\"DeviceName\":\"/dev/xvdbb\",\"Ebs\":{\"DeleteOnTermination\":true}}]"
         PV="$DEVNAME /dev/xvdbb"
    fi

    local VOL1=$(aws ec2 create-volume --availability-zone $ZONE --size $SZ1 --volume-type $VT|grep -Po 'vol-[^"]+')

    #wait for volume available
    STATE=$(aws --output text ec2 describe-volumes --volume-ids $VOL1 --query Volumes[].State)
    while [ $STATE != "available" ];do sleep 1;STATE=$(aws --output text ec2 describe-volumes --volume-ids $VOL1 --query Volumes[].State);done;

    aws ec2 attach-volume --volume-id $VOL1 --device $DEVNAME --instance-id $IID && \
    sleep 10 && aws ec2 modify-instance-attribute --instance-id $IID --block-device-mappings "[{\"DeviceName\":\"$DEVNAME\",\"Ebs\":{\"DeleteOnTermination\":true}}]" && \
    pvcreate $PV && \
    vgcreate vg_tmp $PV && \
    lvcreate -l 100%VG --wipesignatures y --name lv_tmp vg_tmp

    if [ -e /dev/vg_tmp/lv_tmp ];then
       mkfs.xfs -f /dev/vg_tmp/lv_tmp && \
       mkdir -p /mnt/adsp/tmp && \
       mount -v /dev/vg_tmp/lv_tmp /mnt/adsp/tmp

    fi

    tag_instance

  fi

  [[ -e /dev/vg_tmp/lv_tmp ]] && export TMP_DIR=/mnt/adsp/tmp


}

function get_tmp(){
 if [ -e /dev/mapper/vg_tmp-lv_tmp ];then
   echo -n $(mount|grep -Po "/dev/mapper/vg_tmp-lv_tmp on \S+"|awk '{print $3}')
 elif [ ! -z $TMP_DIR ];then
   echo -n $TMP_DIR
 else
   echo -n "$RESULTS_DIR/tmp/$SM"
 fi

}

function get_max_mem(){
  local HOST=$(detect_hostname)
  local MEM=$(qconf -se $HOST | grep -Po "h_vmem\S+"|sed 's/h_vmem=//')
  MEM=$[ $MEM - 1 ]
  echo -n $MEM
}

function get_targets(){
  if [ "$ENABLE_DB" != "1" ];then
   return
  fi

  local PRJ_ID=$1
  local SM=$2
  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$

  local TARGET=$(curl -sS "$API_HOST/v1/sample/get-target/$PRJ_ID/$SM"|grep -Po 'value":"[^"]+'|sed 's/value":"//'|sed 's/\\//g')
  if [ ! -z "$TARGET" ];then
    echo -n $TARGET
  else
    echoerr "Empty \$TARGET"
  fi

}