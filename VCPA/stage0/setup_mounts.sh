#!/bin/bash
echo "Setting up mounts"

for DEV in $(fdisk -l |grep -Po "^Disk /dev/xv\w+"|sed 's/Disk //');do
 pvcreate $DEV
done;

for DEV in $(fdisk -l |grep -Po "^Disk /dev/nvme\w+"|sed 's/Disk //');do
 pvcreate $DEV
done;

vgcreate vg1 `pvs --noheadings --rows|head -1`
PVs=$(vgs|grep vg1|awk '{print $2}')
DINT=$[ $PVs / 2 ]
SIZE1=$DINT
SIZE2=$[ $PVs - $DINT ]

lvcreate -vi $SIZE1 -l 50%VG --wipesignatures y --name lv_bam vg1
lvcreate -vi $SIZE2 -l 50%VG --wipesignatures y --name lv_results vg1

if [ -e /dev/vg1/lv_bam ];then
   mkfs.xfs  /dev/vg1/lv_bam && \
   mkdir -p /mnt/adsp/bam && \
   mount -v /dev/vg1/lv_bam /mnt/adsp/bam
fi

if [ -e /dev/vg1/lv_results ];then
   mkfs.xfs  /dev/vg1/lv_results && \
   mkdir -p /mnt/adsp/results && \
   mount -v /dev/vg1/lv_results /mnt/adsp/results
fi


