# 
# has_idx
# checks whether bam/cram has index
#
# @param SEQ - bam/cram file name + full path
# @return true/false
function has_idx(){
local SEQ="$1"
 [[ "$SEQ" == *.bam ]] && local SUFFIX=".bam" && local IDXSUFFIX=".bai"
 [[ "$SEQ" == *.cram ]] && local SUFFIX=".cram" && local IDXSUFFIX=".crai"

 BAI="${SEQ/$SUFFIX/$IDXSUFFIX}" #alternative index naming
 if [ -e "${SEQ}$IDXSUFFIX" ] -o [ -e "$BAI" ];then
    echo 1
 else
    echo 0
 fi

}
#
# mk_seqfile_idx
# make a bam/cram index if it doesn't exist
# @param SEQ - bam/cram file name + full path
function mk_seqfile_idx(){
  local SEQ="$1"

  if [ ! $(has_idx $SEQ) ];then
     # chk sambam exists
     if [ ! -Z $SAMBAMBAM ];then
        $SAMBAMBA index "$SEQ"
     elif hash sambamba 2>/dev/null;then
        sambamba index "$SEQ"
     fi
  fi

}

function readgroup_cnt(){
 local SEQ="$1"
 echo $($SAMTOOLS view -H "$SEQ" | grep -c "^@RG")

}
# get_readgroups
# Get read groups as array
# @param SEQ - bam/cram file name + full path
# @param array_list - variable to set results
function get_readgroups(){
 local SEQ="$1"

 if [ ! -z "$SAMTOOLS" ];then
   alist=$($SAMTOOLS view -H "$SEQ" |grep "^@RG"|grep -Po "ID:\S+"|sed 's/ID://')
   echo "${alist[@]}"
 else
  echo "error"
 fi
}

# has_OQ
# does it have original quality score tags
function has_OQ(){
 local SEQ="$1"
 echo $($SAMTOOLS view "$SEQ" |head -1000|grep -c "OQ:")
}

function check_seqfile(){
  local SEQFILE="$1"

  [[ -z "$SAMBAMBA" ]] && exit 1
  local OUTPUT=$(($SAMBAMBA flagstat "$SEQFILE" | head -n 1 1>"${SEQFILE/.bam/.flagstat}") 2>&1)

  if [ -z "$OUTPUT" ];then
    echo 1
  else
    echo 0
  fi
}

function get_read_size(){
 local BAM="$1"
 [[ -z "$BAM" ]] && exit 1
 echo -n $($SAMTOOLS view "$BAM" | head -1 | awk '{print length($10);}')
}

# add_generic_read_group
# append a generic read group id to BAM header.
# requires space to duplicate the original BAM
# @param BAM - bam file name + full path
function add_generic_read_group(){
 local BAM="$1"
 [[ -z "$BAM" ]] && exit 1
 local B=$(basename $BAM)
 local SM=${B/.bam/}

 local DIR1=$(dirname $BAM)
 local HFILE=$DIR1/$SM.header

 # extract header, append @RG
 $SAMTOOLS view -H "$BAM" > $HFILE && \
 echo -e "@RG\tID:A\tLB:A\tPL:ILLUMINA\tSM:$SM" >> $HFILE

 # reheader
 local OBAM=${BAM/.bam/.old.bam}
 mv $BAM $OBAM && \
 $SAMTOOLS reheader -P $HFILE $OBAM > $BAM && \
  rm -v $OBAM
 if [ -s "$BAM" ];then
    echo 1
 else
    echo 0
 fi
}

function isCRAM(){
 local SEQ="$1"

 if [[ "$SEQ" == *.cram ]];then
   echo -n 1
 else
   echo -n 0
 fi
}


function CRAM2BAM(){
 local CRAM="$1"
 local BAM="$2"
 local REF_FASTA="$3"

 [[ -z "$REF_FASTA" ]] && exit 1
 if [ ! -z $MAX_THREADS ];then
    MAX_THREADS=$(getconf _NPROCESSORS_ONLN)
 fi

 samtools view -b -T $REF_FASTA -@ $MAX_THREADS -o $BAM $CRAM

 if [ -s "$BAM" ];then
    echo 1
 else
    echo 0
 fi

}

function isSorted(){
 local SEQ="$1"

 LIST_A=$(samtools view $SEQ|head -1000|cut -f3-4)
 LIST_B=$(samtools view $SEQ|head -1000|cut -f3-4|sort)

 if [ "$LIST_A" == "$LIST_B" ];then
   echo -n 1
 else
   echo -n 0
 fi
}